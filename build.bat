@echo ON

rmdir /Q /S build
mkdir build
pushd build

cmake -G "Visual Studio 15 2017" -A Win32 ..
cmake --build . --config Release
ctest .

popd