#pragma once
#ifndef REPL_H
#define REPL_H

/*!
\brief Цикл исполнения команд в консоли.

\details REPL-консоль, обрабатывающая команды для манипулирования над набором данных.
*/
void REPL() noexcept;

#endif //REPL_H

