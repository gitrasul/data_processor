#include <repl.h>

#include <data_manager/data_manager.h>

#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <algorithm>
#include <memory>
#include <cctype>


namespace
{

using ArgsList = std::vector<std::string>;
using CommandAndArgs = std::pair<std::string, ArgsList>;

std::unique_ptr<data_manager::DataManager> g_dataManager; ///< Прокси, предоставляющий операции над данными


/*!
\brief Проверяет, является ли строка тройкой различных букв или цифр.

\param[in] str Проверяемая строка.

\return Возвращает true, если строка соответствует предикату, иначе false.

\note Свойство проверяется на основе текущей C-локали, т.е. по умолчанию соответствуют
английские буквы обоих регистров и цифры.
*/
bool UniqueAlphanumTriad( const std::string& str ) {
    bool isTriad = ( str.size() == 3 );
    if( !isTriad ) {
        return false;
    }

    bool hasOnlyAlphanum = std::all_of( str.cbegin(), str.cend(), isalnum );
    if( !hasOnlyAlphanum ) {
        return false;
    }

    bool hasOnlyUnique = true;
    for( auto ch : str ) {
        if( 1 < std::count( str.cbegin(), str.cend(), ch ) ) {
            hasOnlyUnique = false;
        }
    }
    return hasOnlyUnique;
}

/*!
\brief Выводит в консоль, как использовать команду установки генерируемых значений.
*/
void PrintSetUsage() {
    std::cout << "wrong args for \'set\' command, you have to pass triad of unique alphas or digits, "
        "like \'set bCa\', or \'set bc4\', etc." << std::endl;
}


/*!
\brief Задает новую тройку генерируемых значений для потока данных и отношения между значениями.

\details Отношения между значениями задаются порядком в тройке - например 'BCA' задает отношение 'B < C < A'.
Если переданы некорректные аргументы, в консоль выводится соответствующее сообщение.
Одно значение будет генериророваться 0.5 секунды.

\param[in] args Список аргументов команды; должен содержать хотя бы один элемент,
и этот элемент должен представлять новую упорядоченную тройку значений.

\note Не влияет на статус потоков генерирования/сортировки данных при переустановке значений,
т.е. если на момент сброса генерирование было активно, оно таковым после операции и останется.
*/
void ExecSet( const ArgsList& args ) {
    if( args.size() < 1 || !UniqueAlphanumTriad( args[ 0 ] ) ) {
        PrintSetUsage();
        return;
    }

    std::vector<char> charList( args[ 0 ].cbegin(), args[ 0 ].cend() );
    const std::chrono::milliseconds GeneratingPeriod( 500 );

    if( g_dataManager ) {
        g_dataManager->ResetKinds( charList, GeneratingPeriod );
    } else {
        g_dataManager = std::make_unique<data_manager::DataManager>( charList, GeneratingPeriod );
    }
}

/*!
\brief Запускает генерацию случайных значений из ранее заданного с помощью ExecSet множества символов.
*/
void ExecGen() {

    if( g_dataManager ) {
        g_dataManager->StartGenerating();
    }
}

/*!
\brief Останавливает генерацию случайных значений.
*/
void ExecNgen() {

    if( g_dataManager ) {
        g_dataManager->StopGenerating();
    }
}


/*!
\brief Запускает сортировку сгенерированных значений.
*/
void ExecSort() {

    if( g_dataManager ) {
        g_dataManager->StartSorting();
    }
}

/*!
\brief Останавливает сортировку значений.
*/
void ExecNsort() {

    if( g_dataManager ) {
        g_dataManager->StopSorting();
    }
}

/*!
\brief Выводит сформировавшийся на текущий момент набор данных.

\note Если активны и генерирующий, и сортирующий потоки, то вывод отсортированного набора значений
не гарантирован, из-за конкуренции этих потоков, поэтому для гарантированного вывода отсортированных 
значений требуется сперва отключить генерацию. Подробнее о нюансе в описании data_manager::DataManager.
*/
void ExecShow() {

    if( g_dataManager ) {
        std::vector<char> data = g_dataManager->GetData();

        std::cout << "data: " << std::flush;
        for( auto ch : data ) {
            std::cout << ch;
        }
        std::cout << std::endl;
    }
}

/*!
\brief Сбрасывает существующий набор данных.

\note Не влияет на статус потоков генерирования/сортировки данных, т.е. если 
на момент сброса генерирование было активно, оно таковым после операции и останется.
*/
void ExecClear() {

    if( g_dataManager ) {
        g_dataManager->ClearData();
    }
}

/*!
\brief Выводит список доступных команд с описанием каждой.
*/
void ExecHelp() {
    std::cout 
        << "set - instruct for generating next data with random values in " 
        "the triad of alphas or digits passed as parameter; "
        "clear all previous generated data, but will not affect generating and sorting status.\n\n"
        << "gen - start generating data with every 0.5 secs per item.\n\n"
        << "ngen - stop generating data.\n\n"
        << "sort - start sorting data according to their relationship - the order of values "
        "in triad passed in \'set\' command.\n\n"
        << "nsort - stop sorting data.\n\n"
        << "show - print existing data; if both generating and sorting are active there is no guarantee "
        "data will print being completely sorted; recommended to call \'ngen\' before printing.\n\n"
        << "clear - clear all existing data; will not affect generating and sorting status.\n\n"
        << "exit - exit from program."
        << std::endl;
}

/*!
\brief Парсит строку как команду и список ее аргументов.

\details Разбивает строку на отдельные слова, при этом формируется пара
из первого слова (команда) и списка остальных слов (аргументы команды).

\param[in] commandLine Строка из слов, разделенных пробелами.

\return Пара из команды и списка ее аргументов.
*/
CommandAndArgs ExtractCommandAndArgs( const std::string &commandLine ) {
    std::istringstream iss( commandLine );
    std::string command;

    if( !( iss >> command ) ) {
        return CommandAndArgs {};
    }

    std::string arg;
    ArgsList args;
    while( iss >> arg ) {
        args.push_back( std::move( arg ) );
    }

    return CommandAndArgs { std::move( command ), std::move( args ) };
}

/*!
\brief Выводит приглашение к вводу следующей команды.
*/
void PrintBeginInput() {
    std::cout << ">> " << std::flush;
}

/*!
\brief Выводит, что передана неизвестная команда, и предлагает посмотреть доступные.
*/
void PrintHelpSuggestion() {
    std::cout << "unknown command, type \'help\' to list available." << std::endl;
}


/*!
\brief Тело REPL интерпретатора.
*/
void DoREPL() {

    PrintBeginInput();
    std::string commandLine;

    while( std::getline( std::cin, commandLine ) ) {
        std::string command;
        ArgsList args;
        std::tie( command, args ) = ExtractCommandAndArgs( commandLine );

        if( command == "" ) {
            //
        } else if( command == "set" ) {
            ExecSet( args );
        } else if( command == "gen" ) {
            ExecGen();
        } else if( command == "ngen" ) {
            ExecNgen();
        } else if( command == "sort" ) {
            ExecSort();
        } else if( command == "nsort" ) {
            ExecNsort();
        } else if( command == "show" ) {
            ExecShow();
        } else if( command == "clear" ) {
            ExecClear();
        } else if( command == "help" ) {
            ExecHelp();
        } else if( command == "exit" ) {

            break;
        } else {
            PrintHelpSuggestion();
        }

        PrintBeginInput();
    }
}

}

void REPL() noexcept {
    try {
        DoREPL();
    } catch( const std::exception& ex ) {
        std::cerr << ex.what() << std::endl;
    } catch( ... ) {
        std::cerr << "REPL: unknown error occurred." << std::endl;
    }
}
