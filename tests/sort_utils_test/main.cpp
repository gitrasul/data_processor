#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <sort_utils/sort_utils.h>

#include <vector>
#include <algorithm>

using namespace sort_utils;

TEST_CASE( "IndexOf testing", "[index_of]" ) {
    const std::vector<int> vec { 1, 9, 4, 5, 10, 2, 5 };

    ///< Типичный случай, два различных уникальных значения в контейнере.
    REQUIRE( IndexOf( vec, 9 ) < IndexOf( vec, 4 ) );

    ///< Сравнение индексов для дважды встречающегося значения.
    REQUIRE( IndexOf( vec, 5 ) == IndexOf( vec, 5 ) );

    ///< Сравнение с индексом значения между двумя экземплярами другого значения.
    REQUIRE( IndexOf( vec, 5 ) < IndexOf( vec, 2 ) );

    ///< Индекс любого значения из контейнера должен быть больше чем у отсутсвующего.
    REQUIRE( IndexOf( vec, 3 ) < IndexOf( vec, 1 ) );

    ///< Индексы любых отсутствующих в контейнере значений должны быть равны.
    REQUIRE( IndexOf( vec, 3 ) == IndexOf( vec, 15 ) );
}

TEST_CASE( "BubbleSort testing", "[bubble_sort]" ) {
    std::vector<int> bsortVec { 5, 9, 3, 5, 6, 4, 1, 5, 3, -1, 10, 0, -4 };
    std::vector<int> stdVec = bsortVec;

    ///< Проверка обычной работы алгоритма сортировки.
    BubbleSort( bsortVec );
    std::sort( stdVec.begin(), stdVec.end() );

    REQUIRE( bsortVec == stdVec );
}

TEST_CASE( "BubbleSort with position comparing", "[index_bubble_sort]" ) {
    ///< Вектор, задающий отношения между значениями порядком их расположения.
    const std::vector<int> kinds = { 3, -1, 2, 1 };

    ///< Вектор с расположением значений по шаблону kinds, требуемый результат от алгоритма для задачи.
    const std::vector<int> neededVec = { 3, 3, 3, -1, 2, 2, 1, 1, 1 };

    std::vector<int> testedVec = { -1, 3, 3, 1, 2, 2, 3, 1, 1 };

    ///< Работа реализованного для задачи алгоритма.
    BubbleSort( testedVec,
        [ &kinds ]( int first, int second ) {
            return IndexOf( kinds, first ) < IndexOf( kinds, second );
        }
    );

    REQUIRE( neededVec == testedVec );
}