#include <data_manager.h>

#include <sort_utils/sort_utils.h>

#include <algorithm>
#include <random>

namespace
{

/*!
\brief Проверяет непустой контейнер на наличие только уникальных значений.

\param[in] values Проверяемый контейнер.

\return Проверяемый контейнер.

\throw std::logic_error Если контейнер не удовлетворяет условиям.
*/
template <typename Elem>
decltype( auto ) UniqueValues( const std::vector<Elem>& values ) {
    bool areUnique = true;
    for( const auto& elem : values ) {
        if( 1 < std::count( values.cbegin(), values.cend(), elem ) ) {
            areUnique = false;
        }
    }

    if( !areUnique || values.empty() ) {
        throw std::logic_error( "UniqueValues : elements must have unique values." );
    }

    return values;
}

/*!
\brief Генерирует случайное значение в заданном диапазоне.

\param[in] first Наименьшее возможное значение.

\param[in] second Наибольшее возможное значение.

\return Случайное значение.
*/
size_t GetRandomInteger( size_t first, size_t last ) {
    std::random_device device;
    std::default_random_engine generator( device() );
    std::uniform_int_distribution<size_t> distribution( first, last );
    const size_t RandomInteger = distribution( generator );

    return RandomInteger;
}

/*!
\brief Данный класс реализует семантику паузы действия, и его восстановления при выходе объекта из скоупа.

\detail Создание класса обосновывается требованием гарантированного восстановления статуса некоего действия, 
даже при возникновении исключений в окружающем коде. Необходим для создания инварианта "статус воркеров" 
при вызове блокирующих методов объекта DataManager.
*/
template<typename PauseFunc, typename ResumeFunc>
class ScopedPause {
public:

    /*!
    \brief Создание объекта, управлюящего статусо некоторого действия.

    \param[in] isRunning Прежний статус действия, восстановление которого гарантирует создаваемый объект.

    \param[in] pause Вызываемый объект для остановки действия.

    \param[in] resume Вызываемый объект для запуска действия.
    */
    ScopedPause( bool isRunning, PauseFunc pause, ResumeFunc resume )
    : m_IsRunning( isRunning ), m_pause( pause ), m_resume( resume ) {
        if( m_IsRunning ) {
            m_pause();
        }
    }

    ScopedPause( const ScopedPause& ) = delete;
    ScopedPause& operator=( const ScopedPause& ) = delete;

    ScopedPause( ScopedPause&& ) = default;
    ScopedPause& operator=( ScopedPause&& ) = default;


    /*!
    \brief Восстанавливает прежний статус действия при разрушении объекта.
    */
    ~ScopedPause() {
        RestoreRunning();
    }

public:

    /*!
    \brief Восстанавливает прежний статус действия.
    */
    void RestoreRunning() {
        if( m_IsRunning ) {
            m_resume();
        }
    }

private:

    const bool m_IsRunning;
    PauseFunc m_pause;
    ResumeFunc m_resume;
};


/*!
\brief Создает объект, гарантирующий восстановление статуса генерирующего воркера DataManager объекта.

\param[in] Объект DataManager, для которого сохраняется статус воркера.

\return Объект, восстанавливающий статус воркера.
*/
inline auto GeneratingScopedPause( data_manager::DataManager& manager) {
    auto pause = [ &manager ] { manager.StopGenerating(); };
    auto resume = [ &manager ] { manager.StartGenerating(); };
    return ScopedPause<decltype(pause), decltype(resume)>( manager.IsGenerating(), pause, resume );
}

/*!
\brief Создает объект, гарантирующий восстановление статуса сортирующий воркера DataManager объекта.

\param[in] Объект DataManager, для которого сохраняется статус воркера.

\return Объект, восстанавливающий статус воркера.
*/
inline auto SortingScopedPause( data_manager::DataManager& manager ) {
    auto pause = [ &manager ] { manager.StopSorting(); };
    auto resume = [ &manager ] { manager.StartSorting(); };
    return ScopedPause<decltype( pause ), decltype( resume )>( manager.IsSorting(), pause, resume );
}

}

namespace data_manager
{

using Lock = std::unique_lock<std::mutex>;

DataManager::DataManager( const std::vector<char>& kinds,
    std::chrono::milliseconds generatingPeriod
)
: m_kinds( UniqueValues( kinds ) )
, m_generatingPeriod( generatingPeriod )
, m_readyGenerate( false )
, m_readySort( false )
, m_exitTime( false )
, m_backgroundException( nullptr )
, m_generatingWorker( &DataManager::GeneratingWork, this )
, m_sortingWorker( &DataManager::SortingWork, this ) {

}

DataManager::~DataManager() {
    FinishWorkers();
}

void DataManager::ResetKinds( const std::vector<char>& newKinds,
    std::chrono::milliseconds generatingPeriod
) {
    ///< Приходится обходить проблему thread starvation явным запросом воркеров на остановку
    ///< перед захватом разделяемого ресурса, и восстановление их прежнего статуса при выходе из функции.
    auto generatingPause = GeneratingScopedPause( *this );
    auto sortingPause = SortingScopedPause( *this );

    {
        Lock lk( m_mtx );
        ThrowBackgroundIfExists();
        m_kinds = UniqueValues( newKinds );
        m_generatingPeriod = generatingPeriod;
        m_data.clear();
    }
}

void DataManager::StartGenerating() noexcept {
    m_readyGenerate = true;
    m_cvGenerating.notify_one();
}

void DataManager::StopGenerating() noexcept {
    m_readyGenerate = false;
}

void DataManager::StartSorting() noexcept {
    m_readySort = true;
    m_cvSorting.notify_one();
}

void DataManager::StopSorting() noexcept {
    m_readySort = false;
}

bool DataManager::IsGenerating() const noexcept {
    return m_readyGenerate.load();
}

bool DataManager::IsSorting() const noexcept {
    return m_readySort.load();
}

std::vector<char> DataManager::GetData() {
    auto generatingPause = GeneratingScopedPause( *this );
    auto sortingPause = SortingScopedPause( *this );

    std::vector<char> data;
    {
        Lock lk( m_mtx );
        ThrowBackgroundIfExists();
        data = m_data;
    }

    return data;
}

void DataManager::ClearData() {
    auto generatingPause = GeneratingScopedPause( *this );
    auto sortingPause = SortingScopedPause( *this );

    {
        Lock lk( m_mtx );
        ThrowBackgroundIfExists();
        m_data.clear();
    }
}

void DataManager::GeneratingWork() noexcept {
    try {
        while( true ) {
            Lock lk( m_mtx );
            m_cvGenerating.wait( lk, [ this ] { return m_readyGenerate.load(); } );

            ///< Пауза перед созданием нового элемента поставлена под блокировку, чтобы 
            ///< при входе в тело цикла как можно быстрее попытаться захватить ресурс, 
            ///< иначе другой воркер будет все время опережать в захвате.
            std::this_thread::sleep_for( m_generatingPeriod );

            if( m_exitTime ) {
                break;
            }

            GenerateItem();
        }
    } catch( ... ) {
        m_backgroundException = std::current_exception();
    }
}

void DataManager::SortingWork() noexcept {
    try {
        while( true ) {
            Lock lk( m_mtx );
            m_cvSorting.wait( lk, [ this ] { return m_readySort.load(); } );

            if( m_exitTime ) {
                break;
            }

            SortData();
        }
    } catch( ... ) {
        m_backgroundException = std::current_exception();
    }
}

void DataManager::GenerateItem() {
    const size_t RandomIndex = GetRandomInteger( 0, m_kinds.size() - 1 );
    char newItem = m_kinds[ RandomIndex ];
    m_data.push_back( newItem );
}

void DataManager::SortData() {
    using namespace sort_utils;
    BubbleSort( m_data,
        [ this ]( char first, char second ) {
            return IndexOf( m_kinds, first ) < IndexOf( m_kinds, second );
        }
    );
}

void DataManager::ThrowBackgroundIfExists() {
    if( m_backgroundException ) {
        std::rethrow_exception( m_backgroundException );
    }
}

void DataManager::FinishWorkers() {
    m_exitTime = true;

    StartGenerating();
    StartSorting();

    m_generatingWorker.join();
    m_sortingWorker.join();
}

}
