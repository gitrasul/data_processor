#pragma once
#ifndef SORT_UTILS_H
#define SORT_UTILS_H

#include <vector>
#include <algorithm>
#include <utility>

namespace sort_utils
{

/*!
\brief Возвращает позицию элемента с нужным значением в векторе.

\param[in] vec Вектор, в котором ищется элемент.

\param[in] elem Значение искомого элемента.

\return Индекс искомого элемента в векторе при наличии, иначе -1.
*/
template <typename Elem>
int IndexOf( const std::vector<Elem> &vec, Elem elem ) {
    const auto KindIt = std::find( vec.cbegin(), vec.cend(), elem );

    if( KindIt == vec.cend() ) {
        return -1;
    } else {
        return std::distance( vec.cbegin(), KindIt );
    }
}

/*!
\brief Сортировка вектора пузырьковым алгоритмом.

\param[in] vec Сортируемый вектор.

\param[in] comp Вызываемый объект, предикат сравнения двух элементов.

\note Автор не уверен, что именно подразумевалось в ТЗ под "ключевые алгоритмы реализовать самостоятельно", 
поэтому просто скопипастил алгоритм. Лучше конечно воспользоваться реализацией быстрой сортировки std::sort.
*/
template <typename Elem, typename Comparer = std::less<Elem> >
void BubbleSort( std::vector<Elem> &vec, Comparer comp = Comparer() ) {
    const int Size = vec.size();

    for( int i = 0; i < Size; ++i ) {
        for( int j = Size - 1; j > i; --j ) {
            if( comp( vec[ j ], vec[ i ] ) ) {
                std::swap( vec[ j ], vec[ i ] );
            }
        }
    }
}

}

#endif // SORT_UTILS_H

